<?php

use Illuminate\Database\Seeder;

class CreatePositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([

            ['title' => 'Директор'],
            ['title' => 'Зам. директора'],
            ['title' => 'Product manager'],
            ['title' => 'Senior'],
            ['title' => 'Middle'],
            ['title' => 'Junior'],
            ['title' => 'Test'],

        ]);
    }
}
