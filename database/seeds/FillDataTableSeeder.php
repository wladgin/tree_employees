<?php

use Illuminate\Database\Seeder;

class FillDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('ru_Ru');

        $director = new \App\Employees();
        $director->name = $faker->name;
        $director->position_id = 1;
        $director->date_started_work = $faker->date();
        $director->salary = $faker->numberBetween(10000, 50000);
        $director->save();

        for ($z = 0; $z < 5; $z++) {
            $this->makeEmployeeTree(2, $director->id, 3);
        }
    }

    public function makeEmployeeTree($currentPosition, $parentId, $nextCount)
    {
        $faker = \Faker\Factory::create('ru_RU');

        $empl = new \App\Employees();

        $empl->name = $faker->name;
        $empl->position_id = $currentPosition;
        $empl->date_started_work = $faker->dateTimeBetween('-10 years')->format('Y-m-d');
        $empl->salary = $faker->numberBetween(10000, 50000);
        $empl->save();

        $rel = new \App\EmployeesRelations();
        $rel->parent_id = $parentId;
        $rel->child_id = $empl->id;
        $rel->save();

        if ($currentPosition < 7) {
            for ($i = 0; $i < $nextCount; $i++) {
                $this->makeEmployeeTree($currentPosition + 1, $empl->id, $nextCount);
            }
        }
    }
}
