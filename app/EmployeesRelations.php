<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeesRelations extends Model
{
    public $timestamps = false;

    protected $fillable = ['parent_id', 'child_id'];

    public function employees()
    {
        return $this->hasOne(Employees::class, 'id', 'child_id');
    }
}
