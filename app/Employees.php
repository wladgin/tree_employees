<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    public $timestamps = false;

    protected $fillable =  ['name', 'position_id', 'date_started_work', 'salary'];

    public function position()
    {
        return $this->hasOne(Positions::class, 'id', 'position_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\EmployeesRelations', 'parent_id', 'id');
    }


    public static function buildChildren($parentModel)
    {
        $tree = [
            'id' => $parentModel['id'],
            'name' => $parentModel['name'],
            'position' => $parentModel->position['title'],
            'date_started_work' => $parentModel['date_started_work'],
            'salary' => $parentModel['salary'],
            'child' => [],
        ];

        $childrens = $parentModel->childrens;

        if (count($childrens)) {
            foreach ($childrens as $children) {
                $tree['child'][] = self::buildChildren($children->employees);
            }
        }

        return $tree;
    }

    public function thumbnails(){
        return $this->belongsToMany(Thumbnail::class);
    }
}
