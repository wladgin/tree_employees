<?php

namespace App\Http\Controllers;

use App\Employees;
use App\EmployeesRelations;
use App\Positions;
use App\Thumbnail;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employees $employees)
    {
        $employees = Employees::all();
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $directors = Employees::query()->where('position_id', '=', 1)->get();

        $tree = [];
        if (count($directors)) {
            foreach ($directors as $director) {
                $tree[] = Employees::buildChildren($director);
            }
        }

        return view('employees.create', [
            'positionsList' => Positions::getList(),
            'tree' => $tree,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(),[
            'name' => 'required|min:10',
            'date_started_work' => 'required|string',
            'position_id' => 'required|integer',
            'salary' => 'required',
        ]);
        //dd(request(['name', 'position_id', 'date_started_work', 'salary']));
        $employees = Employees::create(request(['name', 'position_id', 'date_started_work', 'salary']));

        if(count(request()->files->get('thumbnail'))){
            foreach (request()->files->get('thumbnail') as $file){

                $file = $file->move(public_path().'/uploads/', time().'_'.$file->getClientOriginalName());

                $thumbnail = Thumbnail::create([
                    'name' => basename($file->getRealPath()),
                    'size' => basename($file->getSize())
                ]);

                $employees->thumbnails()->attach($thumbnail->id);


            }
        }
        //dd(['parent_id' => request('parent_id'),'child_id' => $employees->id]);
        EmployeesRelations::create(['parent_id' => request('parent_id'),'child_id' => $employees->id]);
        return redirect('/employees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $directors = Employees::query()->where('position_id', '=', 1)->get();

        $tree = [];
        if (count($directors)) {
            foreach ($directors as $director) {
                $tree[] = Employees::buildChildren($director);
            }
        }

        $employees = Employees::select()->where('id', '=', $id)->first();

        return view('employees.edit', [
            'employees' => $employees,
            'positionsList' => Positions::getList(),
            'tree' => $tree,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $employees = Employees::select()->where('id', '=', $id)->first();

        $this->validate(request(),[
            'name' => 'required|min:10',
            'date_started_work' => 'required|string',
            'position_id' => 'required|integer',
            'salary' => 'required',
        ]);

        if(count(request()->files->get('thumbnail'))){
            foreach (request()->files->get('thumbnail') as $file){

                $file = $file->move(public_path().'/uploads/', time().'_'.$file->getClientOriginalName());

                $thumbnail = Thumbnail::create([
                    'name' => basename($file->getRealPath()),
                    'size' => basename($file->getSize())
                ]);

                $employees->thumbnails()->attach($thumbnail->id);
            }
        }

        $employees->update(request(['name', 'position_id', 'date_started_work', 'salary']));

        return redirect('/employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmployeesRelations::where('child_id', '=', $id)->delete();
        $employees = Employees::select()->where('id', '=', $id)->first();
        $employees->thumbnails->first()->delete();
        $employees->delete();


        return redirect('/employees');
    }
}
