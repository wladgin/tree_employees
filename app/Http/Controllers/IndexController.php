<?php

namespace App\Http\Controllers;

use App\Employees;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $directors = Employees::query()->where('position_id', '=', 1)->get();

        $tree = [];
        if (count($directors)) {
            foreach ($directors as $director) {
                $tree[] = Employees::buildChildren($director);
            }
        }

        return view('home.index', ['tree' => $tree]);
    }
}
