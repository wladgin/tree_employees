<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
//    protected $table = 'positions';

    public $timestamps = false;

    protected $fillable = ['id', 'title'];

    public static function getList()
    {
        $positions = self::all();

        $list = [];

        foreach ($positions as $position) {
            $list[$position['id']] = $position['title'];
        }

        return $list;
    }
}
