<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="/">Test</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>

            @if(Auth::check())

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <a class="dropdown-item" href="/employees">Все сотрудники <span class="sr-only">(current)</span></a>
                        <a class="dropdown-item" href="/employees/create">Добавить сотрудника</a>
                    </div>
                </li>


                <li class="nav-item active">
                    <a class="nav-link" href="/logout">Logout <span class="sr-only">(current)</span></a>
                </li>

            @else
                <li class="nav-item active">
                    <a class="nav-link" href="/login">Sing in <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="/register">Registration <span class="sr-only">(current)</span></a>
                </li>
            @endif
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" id="text-to-find" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0"
                    onclick="javascript: FindOnPage('text-to-find'); return false;" type="submit">Search
            </button>
        </form>
    </div>
</nav>