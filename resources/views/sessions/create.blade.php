@extends('template')
@section('content')

    <form class="form-signin col-md-4 " style="margin-left:33%" method="post" action="/sessions">

        {{csrf_field()}}

        @include('embed.errors')

        <div class="mb-5">
            <h1 class="font-weight-normal mb-5" align="center">Please sign in</h1>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required
                   autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password"
                   required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </div>

    </form>



@endsection
