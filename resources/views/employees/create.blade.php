@extends('template')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Добавить нового сотрудника:</h1>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-md-6">

        <form action="/employees/create" method="post" class="form-horizontal" enctype="multipart/form-data">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">

                <label for="name">ФИО:</label>
                <input type="text" name="name" id="name" class="form-control">

            </div>
            <div class="form-group">

                <label for="position_id">Должность:</label>
                <select name="position_id" id="position_id" class="form-control">
                    <option value="">Выберите должность</option>
                    @foreach($positionsList as $id => $title)
                        <option value="{{$id}}">{{$title}}</option>
                    @endforeach
                </select>

            </div>

            <div class="form-group">

                <label for="parent_id">Начальник:</label>
                <select name="parent_id" id="parent_id" class="form-control">
                    <option value="">Выберите должность</option>
                    @foreach ($tree as $item)
                        @include('employees.part', array('tree' => $item))
                    @endforeach
                </select>

            </div>

            <div class="form-group">

                <label for="thumbnail"> Фото: </label>
                <input type="file" class="form-control" multiple name="thumbnail[]" id="thumbnail">

            </div>

            <div class="form-group">

                <label for="date_started_work">Дата приема на работу:</label>
                <input type="text" name="date_started_work" value="{{date('Y-m-d')}}" id="date_started_work" class="form-control">

            </div>

            <div class="form-group">

                <label for="salary">Зарплата:</label>
                <input type="text" name="salary" id="salary" class="form-control">

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>


        </form>
    </div>

@endsection
