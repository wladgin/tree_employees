@extends('template')

@section('jumbotron')


    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Добро пожаловать!</h1>
            <p> Информация обо всех сотрудников: </p>
        </div>
    </div>
@endsection

@section('content')

    <main role="main">

        <div class="container">

            <div class="row">
                <h2>Сотрудники:</h2>
            </div>

            <div class="col-xs-12">
                <table class="table table-hovered table-bordered sort">
                    <thead>
                    <tr>
                        <td>№</td>
                        <td>ФИО</td>
                        <td>Должность</td>
                        <td>Дата ​приема ​на ​работу</td>
                        <td>Размер ​заработной ​платы</td>
                        <td>Фото</td>
                        <td>Редактировать/Удалить</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <td>{{ $employee['id'] }}</td>
                            <td>{{ $employee['name'] }}</td>
                            <td>{{ $employee->position['title'] }}</td>
                            <td>{{ $employee['date_started_work'] }}</td>
                            <td>{{ $employee['salary'] }}</td>
                            <td><img class="card-img-top" src="/uploads/{{$employee->thumbnails->first()['name']}}" alt="" style="width: 150px; height: 150px" ></td>
                            <td>
                                <a href="/employees/{{$employee['id']}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="/employees/{{$employee['id']}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <hr>

        </div>

    </main>
    <script src="js/sort.js"></script>
@endsection

