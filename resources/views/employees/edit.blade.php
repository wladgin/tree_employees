@extends('template')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Редактировать сотрудника:</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="col-md-12">

        <form action="/employees/{{$employees['id']}}" method="post" class="form-horizontal">

            {{--{{method_field('PUT')}}--}}

            @include('embed.errors')

            {{csrf_field()}}


            <div class="form-group">
                <p style="text-align: center">
                    <img class="card-img-top" src="/uploads/{{$employees->thumbnails->first()['name']}}" alt="" style="width: 150px; height: 200px" ><br>
                </p>
                <label for="name">ФИО:</label>
                <input type="text" name="name" value="{{$employees['name']}}" id="name" class="form-control">

            </div>
            <div class="form-group">

                <label for="position_id">Должность:</label>
                <select name="position_id" id="position_id" class="form-control">
                    <option value="">Выберите должность</option>
                    @foreach($positionsList as $id => $title)
                        <option value="{{$id}}"{{$employees['position_id'] == $id ? ' selected="true"' : ''}}>{{$title}}</option>
                    @endforeach
                </select>

            </div>

            <div class="form-group">

                <label for="parent_id">Начальник:</label>
                <select name="parent_id" id="parent_id" class="form-control">
                    <option value="">Выберите должность</option>
                    @foreach ($tree as $item)
                        @include('employees.part', array('tree' => $item))
                    @endforeach
                </select>

            </div>

            <div class="form-group">

                <label for="thumbnail"> Фото: </label>
                <input type="file" class="form-control" multiple name="thumbnail[]" id="thumbnail">

            </div>

            <div class="form-group">

                <label for="date_started_work">Дата приема на работу:</label>
                <input type="date" id="date_started_work" value="{{$employees['date_started_work']}}" name="date_started_work">

            </div>

            <div class="form-group">

                <label for="salary">Зарплата:</label>
                <input type="text" name="salary" value="{{$employees['salary']}}" id="salary" class="form-control">

            </div>

            <div class="form-group">
                <button class="btn btn-default">Save</button>
            </div>


        </form>
    </div>

@endsection
