<option value="{{$tree['id']}}"><b>{{$tree['name']}} Position</b>: {{ $tree['position'] }}</option>
    @if(count($tree['child'] > 0))
            @foreach ($tree['child'] as $item)
                @include('employees.part', array('tree' => $item))
            @endforeach
    @endif
