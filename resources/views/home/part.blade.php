<li>
    {{ $tree['name'] }}
    <span><b>Position</b>: {{ $tree['position'] }}</span>
    <span><b>Date Started Work</b>: {{ $tree['date_started_work'] }}</span>
    <span><b>Salary</b>: {{ $tree['salary'] }}</span>
    @if(count($tree['child'] > 0))
        <ul>
            @foreach ($tree['child'] as $item)
                @include('home.part', array('tree' => $item))
            @endforeach
        </ul>
    @endif
</li>