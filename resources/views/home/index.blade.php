@extends('template')

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Добро пожаловать!</h1>
            <p> Веб страница, которая выводит иерархию сотрудников в древовидной ​форме. </p>
            <ul>
                <li>Информация о каждом сотруднике должна храниться в базе данных и содержать ​следующие ​данные:</li>
                <ul>
                    <li>ФИО;</li>
                    <li>Должность;</li>
                    <li>Дата ​приема ​на ​работу;</li>
                    <li>Размер ​заработной ​платы;</li>
                </ul>
                <li>У ​каждого ​сотрудника ​есть ​1 ​начальник;</li>
                <li>База данных должна содержать не менее 50 000 сотрудников и 5 уровней иерархий.</li>
                <li>Не ​забудьте ​отобразить ​должность ​сотрудника.</li>
            </ul>
            @if(Auth::check())
                <p><a class="btn btn-primary btn-lg" href="/employees" role="button">Все сотрудники &raquo;</a></p>
            @endif
        </div>
    </div>
@endsection

@section('content')

    <main role="main">

        <div class="container">

            <div class="row">
                <h2>Дерево сотрудников:</h2>
            </div>

            <div>
                <ul>
                    @foreach ($tree as $item)
                        @include('home.part', array('tree' => $item))
                    @endforeach
                </ul>
            </div>

            <hr>

        </div>

    </main>

@endsection

