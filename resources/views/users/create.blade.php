@extends('template')
@section('content')


    <form class="form-signin col-md-4 " style="margin-left:33%" method="post" action="/register">

        {{csrf_field()}}

        @include('embed.errors')


        <div class="mb-5">
            <h1 class="font-weight-normal mb-5">Registration</h1>
            <div>
                <label for="name" class="sr-only">Full name:</label>
                <input type="name" id="name" name="name" class="form-control" placeholder="Your full name" required
                       autofocus>
            </div>

            <div>
                <label for="inputEmail" class="sr-only">Email address:</label>
                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required
                       autofocus>
            </div>

            <div>
                <label for="inputPassword" class="sr-only">Password:</label>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password"
                       required>
            </div>

            <div>
                <label for="password_confirmation" class="sr-only">Password conformation:</label>
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control"
                       placeholder="Password conformation" required>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </div>
        <hr>

    </form>



@endsection
