<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create')->name('login');
Route::post('/sessions', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/employees', 'EmployeesController@index');
Route::get('/employees/create', 'EmployeesController@create');
Route::post('/employees/create', 'EmployeesController@store');
Route::get('/employees/{id}/edit', 'EmployeesController@edit');
Route::post('/employees/{id}', 'EmployeesController@update');
Route::get('/employees/{id}', 'EmployeesController@destroy');